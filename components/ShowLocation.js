import React, { useState, useEffect } from 'react'
import { Table } from 'react-bootstrap'
import LocationRow from './LocationRow'

export default function ShowLocation() {
    const [locations, setLocations] = useState([])

    useEffect(() => {
        fetch('https://5f3430949124200016e18826.mockapi.io/api/locations')
            .then(res => res.json())
            .then(data => {
                setLocations(data)
            })
    }, [])


    return (
        <Table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Location</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <LocationRow locations={locations} />
            </tbody>
        </Table>
    )
}
