import React, { useState } from 'react'
import { Button, Row, Col } from 'react-bootstrap'
import Router from 'next/router'

export default function LocationRow({ locations }) {

    const [showModal, setShowModal] = useState(false)

    function updateEntry(locId) {
            console.log("updateEntry")
            Router.push({
                pathname: 'locations/edit',
                query: {
                    locId: locId
                }
            }
        )    
    }

    function deleteEntry(locId) {
        fetch(`https://5f3430949124200016e18826.mockapi.io/api/locations/${locId}`, {
            method: 'DELETE'
        })
            .then(res => res.json())
            .then(data => {
                if (data.id != null) {
                    Router.reload();
                }
            })

    }
    
    return (
        locations.map(location => {
            return <tr key={location.id}>
                <td>{location.id}</td>
                <td>{location.location}</td>
                <td>{location.description}</td>
                <td>
                    <Row>
                        <Col xs={12} lg={6} align="center">
                            <Button variant="success" onClick={() => updateEntry(location.id)}>Update</Button>

                        </Col>
                        <Col xs={12} lg={6} align="center">
                            <Button variant="danger" onClick={() => deleteEntry(location.id)}>Delete</Button>
                        </Col>
                    </Row>
                </td>
            </tr>
        })
    )
}
