import React, { useState } from 'react'

export default function LocationForm() {

    const [location, setLocation] = useState("")
    const [description, setDescription] = useState("")

    function addLocation(event) {
        event.preventDefault()
        fetch('https://5f3430949124200016e18826.mockapi.io/api/locations', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                location: location,
                description: description
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.id != null) {
                    window.location.reload();
                }
            })
    }

    return (
        <form onSubmit={(event) => addLocation(event)}>
            Location:
            <input value={location} onChange={(e) => setLocation(e.target.value)} className="form-control" />
            Description:
            <input value={description} onChange={(e) => setDescription(e.target.value)} className="form-control" />
            <br />
            <button className="form-control btn btn-primary">Submit</button>
        </form>
    )
}
