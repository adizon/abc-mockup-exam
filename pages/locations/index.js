import React from 'react'
import { Jumbotron, Container, Row, Col } from 'react-bootstrap'
import LocationForm from '../../components/LocationForm'
import ShowLocation from '../../components/ShowLocation'

export default function locations() {
    return (
        <>
            <Jumbotron>
                <Container>
                    <h1>List of Locations</h1>
                </Container>
            </Jumbotron>
            <Container>
                <Row>
                    <Col xs={12} lg={4}>
                        <LocationForm action={"Add"} />
                    </Col>
                    <Col xs={12} lg={8}>
                        <ShowLocation />
                    </Col>
                </Row>
            </Container>
        </>
    )
}
