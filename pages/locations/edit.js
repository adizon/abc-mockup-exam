import React, { useEffect, useState } from 'react'
import  Router, { useRouter } from 'next/router'
import {Container, Jumbotron} from 'react-bootstrap'

export default function edit() {
    const router = useRouter()
    const { locId } = router.query

    const [location, setLocation] = useState("")
    const [description, setDescription] = useState("")


    useEffect(() => {
        fetch(`https://5f3430949124200016e18826.mockapi.io/api/locations/${locId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setLocation(data.location)
                setDescription(data.description)
            })
    }, [])

    function updateLocation (e) {
        e.preventDefault()
        fetch(`https://5f3430949124200016e18826.mockapi.io/api/locations/${locId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                location: location,
                description: description
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                Router.push('/locations')
            })
    }

    return (
        <Container>
            <Jumbotron>
                <h3>Update Details for Location Number: {locId} </h3>
            </Jumbotron>
        <form onSubmit={(event) => updateLocation(event)}>
            Location:
            <input value={location} onChange={(e) => setLocation(e.target.value)} className="form-control" />
            Description:
            <input value={description} onChange={(e) => setDescription(e.target.value)} className="form-control" />
            <br />
            <button className="form-control btn btn-primary">Submit</button>
        </form>
        </Container>
    )
}
